### Step to run

1. Remember select credential file at **current session shell**: `export GOOGLE_APPLICATION_CREDENTIALS="path_to/digime-ai.json"`
2. Run file: `python main.py -f dataset/1_1.jpg -fo data_example.json`

----

Try to run: `python main.py -h| --help` to show a help.
```text
usage: main.py [-h] [-f FRONT] [-b BACK] [-fo OUT_FILE]
    optional arguments:
      -h, --help            show this help message and exit
      -f FRONT, --front FRONT
                            FRONT card image path
      -b BACK, --back BACK  BACK card image path
      -fo OUT_FILE, --out-file OUT_FILE
                            output file path. If NOT, it is written to out/data_$CURRENT_TIMESTAMP.json

``` 