from argparse import ArgumentParser


def init_arg():
    parser = ArgumentParser()
    parser.add_argument("-f", "--front",
                        help="FRONT card image path")
    parser.add_argument("-b", "--back", default=None,
                        help="BACK card image path")
    parser.add_argument("-fo", "--out-file", default=None,
                        help="output file path. If NOT, it is written to out/data_$CURRENT_TIMESTAMP.json")

    args = parser.parse_args()
    return args
