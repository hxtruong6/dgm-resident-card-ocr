def process_date(date_string):
    date = str(date_string).strip().split('/')
    return {
        'day': int(date[0]),
        'month': int(date[1]),
        'year': int(date[2])
    }


def process_concat_string(str_1, str_2):
    return (str(str_1 or '') + ' ' + str(str_2)).strip()


class CardInformation:
    def __init__(self, date_of_expity=None, card_number=None, full_name=None, date_of_birth=None, sex=None,
                 nationality=None, place_of_origin=None, place_of_residence=None):
        self.date_of_expity = date_of_expity
        self.card_number = card_number
        self.full_name = full_name
        self.date_of_birth = date_of_birth
        self.sex = sex
        self.nationality = nationality
        self.place_of_origin = place_of_origin
        self.place_of_residence = place_of_residence
        self.personal_identification = None
        self.issue_date = None  # dd/mm/yyyy
        self.code_string = None  # last 3 code lines

        self.current_retrieving_state = {
            "date_of_expity": False,
            "card_number": False,
            "full_name": False,
            "date_of_birth": False,
            "sex": False,
            "nationality": False,
            "place_of_origin": False,
            "place_of_residence": False,
            "personal_identification": False,
            "issue_date": False,
            "code_string": False
        }

    def _reset_retrieval_state(self):
        for key in self.current_retrieving_state:
            self.current_retrieving_state[key] = False

    def _update_retrieval_state(self, field):
        self._reset_retrieval_state()
        self.current_retrieving_state[field] = True

    def retrieving_info(self, texts):
        lines = texts.split('\n')
        for i, line in enumerate(lines):
            line = str(line).strip()

            if ("giá trị" or "expiry") in line.lower():
                self._update_retrieval_state("date_of_expity")
                # print(f"date_of_expity: {line.split(':')[1]}")
                s = line.split(':')
                if len(s) == 2:
                    self.date_of_expity = process_date(s[1])
                self._reset_retrieval_state()

            elif ("no." or "số") in line.lower():
                self._update_retrieval_state("card_number")
                # print(f"card_number: {line.split(':')[1]}")
                s = line.split(':')
                if len(s) == 2:
                    self.card_number = str(s[1]).strip()
                self._reset_retrieval_state()

            elif ("họ và tên" or "full name") in line.lower():
                self._update_retrieval_state("full_name")
                s = line.split(':')
                if len(s) == 2 and len(s[1]) > 2:
                    self.full_name = str(s[1]).strip()
                    # NOTE this: only RESET if value is in same line with fullname: Nguyen Van A
                    self._reset_retrieval_state()

            elif self.current_retrieving_state['full_name'] and len(line) > 2:
                # means in current state that would be had "full_name" attribute
                # expect length of string should be longer than 2 characters
                self.full_name = line
                self._reset_retrieval_state()

            elif ("ngày sinh" or "birth") in line.lower():
                self._update_retrieval_state("date_of_birth")
                s = line.split(':')
                if len(s) == 2:
                    self.date_of_birth = process_date(s[1])
                self._reset_retrieval_state()

            elif ("giới tính" or "sex") in line.lower():
                self._update_retrieval_state("sex")
                s = line.split(':')
                if len(s) == 3:
                    # only obtain the first value such as: nam, nu, trai, gai, male, female
                    self.sex = s[1].split()[0].strip()
                    self.nationality = s[2].strip()
                self._reset_retrieval_state()

            elif ("quê quán" or "origin") in line.lower():
                self._update_retrieval_state("place_of_origin")
                s = line.split(':')
                if len(s) == 2:
                    # only obtain the first value such as: nam, nu, trai, gai, male, female
                    self.place_of_origin = s[1].strip()

            elif ("thường trú" or "residence") in line.lower():
                self._update_retrieval_state("place_of_residence")
                s = line.split(':')
                if len(s) == 2:
                    self.place_of_residence = s[1].strip()

            elif ("đặc điểm" or "personal identification") in line.lower():
                self._update_retrieval_state("personal_identification")

            elif ("ngày, tháng" or "date, month") in line.lower():
                self._update_retrieval_state("issue_date")
                s = line.split(':')
                if len(s) == 2:
                    self.issue_date = process_date(s[1])
                self._reset_retrieval_state()

            # NOTE This is low priority to handle value
            elif self.current_retrieving_state['place_of_origin'] and len(line) > 2:
                self.place_of_origin = process_concat_string(self.place_of_origin, line)

            elif self.current_retrieving_state['place_of_residence'] and len(line) > 2:
                self.place_of_residence = process_concat_string(self.place_of_residence, line)

            elif self.current_retrieving_state['personal_identification'] and len(line) > 2:
                self.personal_identification = process_concat_string(self.personal_identification, line)

            elif len(line) > 25 and len(lines[i + 1]) > 25 and len(lines[i + 2]) > 25 \
                    and len(line) == len(lines[i + 1]):
                self._update_retrieval_state("code_string")
                self.code_string = {
                    "full_code": line + lines[i + 1] + lines[i + 2],
                    "line_1": line,
                    "line_2": lines[i + 1],
                    "line_3": lines[i + 2],
                }
                self._reset_retrieval_state()

    def get_data(self):
        return {
            "date_of_expity": self.date_of_expity,
            "card_number": self.card_number,
            "full_name": self.full_name,
            "date_of_birth": self.date_of_birth,
            "sex": self.sex,
            "nationality": self.nationality,
            "place_of_origin": self.place_of_origin,
            "place_of_residence": self.place_of_residence,
            "personal_identification": self.personal_identification,
            "issue_date": self.issue_date,
            "code_string": self.code_string
        }
