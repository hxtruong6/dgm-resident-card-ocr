import io
import os
import time
import json

from google.cloud import vision

import arg_parser
from card_information import CardInformation


def _detect_text(client, image, imageContext=None):
    '''
        detect util function

    '''
    # response = client.text_detection({
    #     "image": {
    #         "source": {
    #             "imageUri": image
    #         }
    #     },
    #     "imageContext": {
    #         "languageHints": [
    #             "vi",
    #             "en"
    #         ]}
    #
    # })
    response = client.text_detection(image)
    texts = response.text_annotations
    # print(texts[0].description)

    # for text in texts:
    #     print('\n"{}"'.format(text.description))
    #
    #     vertices = (['({},{})'.format(vertex.x, vertex.y)
    #                  for vertex in text.bounding_poly.vertices])
    #
    #     print('bounds: {}'.format(','.join(vertices)))

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))

    return texts


def obj2json_file(data, file_path):
    with open(file_path, 'w', encoding='utf-8') as fo:
        json.dump(data, fo, ensure_ascii=False, indent=4)


def detect_image_text(client, file_path):
    with io.open(file_path, 'rb') as image_file:
        content = image_file.read()
    image = vision.Image(content=content)
    # context = vision.ImageContext({"language_hints": ["vi", "en"]})
    texts = _detect_text(client=client, image=image)
    return texts


def main(front_card_path, back_card_path, result_path=None):
    try:
        client = vision.ImageAnnotatorClient()
        card_info = CardInformation()

        if front_card_path:
            texts = detect_image_text(client=client, file_path=front_card_path)
            card_info.retrieving_info(texts[0].description)
        if back_card_path:
            texts = detect_image_text(client=client, file_path=back_card_path)
            card_info.retrieving_info(texts[0].description)

        info_data = card_info.get_data()
        # print(f"Info data {info_data}")
        if result_path:
            obj2json_file(info_data, result_path)

    except Exception as e:
        print(e)


if __name__ == "__main__":
    front_card_path = "dataset/1_1.jpg"
    back_card_path = "dataset/2_1.jpg"

    args = arg_parser.init_arg()
    print(args)
    data_file_path = args.out_file if args.out_file else f"out/data_{int(time.time())}.json"

    # main(front_card_path, back_card_path, data_file_path)
    print("Processing...")
    main(args.front, args.back, data_file_path)
    print("Done")
